from iroha import Iroha, IrohaGrpc, IrohaCrypto

iroha = Iroha('admin@test')
net = IrohaGrpc('127.0.0.1:50051')
admin_priv_key = 'f101537e319568c765b2cc89698325604991dca57b9716b58016b253506cab70'


def create_asset():
    commands = [
        iroha.command('CreateAsset', asset_name='whatcoin', domain_id='test',
                      precision=5)
    ]
    tx = IrohaCrypto.sign_transaction(iroha.transaction(commands), admin_priv_key)
    net.send_tx(tx)
    for status in net.tx_status_stream(tx):
        print(status)


def add_asset_qty():
    commands = [
        iroha.command('AddAssetQuantity', asset_id='whatcoin#test', amount='10')
    ]
    tx = IrohaCrypto.sign_transaction(iroha.transaction(commands), admin_priv_key)
    net.send_tx(tx)
    for status in net.tx_status_stream(tx):
        print(status)


def transfer_asset():
    commands = [
        iroha.command('TransferAsset', src_account_id='admin@test',
                      dest_account_id='test@test', asset_id='whatcoin#test',
                      amount='4')
    ]
    tx = IrohaCrypto.sign_transaction(iroha.transaction(commands), admin_priv_key)
    net.send_tx(tx)
    for status in net.tx_status_stream(tx):
        print(status)


def query_balance(account):
    print(f'{account} balance:')
    query = iroha.query('GetAccountAssets', account_id=account)
    IrohaCrypto.sign_query(query, admin_priv_key)
    response = net.send_query(query)
    for asset_data in response.account_assets_response.account_assets:
        print(f'{account} has {asset_data.asset_id} - {asset_data.balance}')


def main():
    create_asset()
    add_asset_qty()
    transfer_asset()
    query_balance('admin@test')
    query_balance('test@test')


if __name__ == '__main__':
    main()
